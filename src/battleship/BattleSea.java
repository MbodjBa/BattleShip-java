/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame
{
    
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    private InetAddress adrs;
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private JTextField txtIP;
    private JRadioButton JRserver;
    private JRadioButton JRclient;
    private  ButtonGroup bg ;
    private Boolean Gameover;
    private BackGroundCom com;
    private Thread t;
   
    public BattleSea()
    {
        this.setSize(500,425);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector();
              this.add(JRclient);  
              this.add(JRserver);
       this.add(txtIP);
        this.add(txtPortNumber);
            this.add(btnStartServer);
         CreateShips();
        
     
       
        
    }
     private static void etat(){
         Boolean etat = false;
    for(int i = 0; i< listbtn.length;i++)
        {
      if(listbtn[i].getBackground()==java.awt.Color.green){
              etat=true;   
      }
                              
        }
    
      }
    
    private void createGrid()
    {
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10,10);
         listbtn[i].setBackground(java.awt.Color.blue);          
         this.add(listbtn[i]);
                              
        }
        
        btnStartServer = new JButton("connect");
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(JRserver.isSelected()){
                     com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
                 t = new Thread(com);
                 t.start();
                }
                else if(JRclient.isSelected()){
                 com = new BackGroundCom(txtIP.getText().toString(),Integer.parseInt(txtPortNumber.getText().toString()));
                 t = new Thread(com);
                 t.start();
                }
            }
        });
         
               
            JRclient = new JRadioButton();
            JRclient.setText("Client");     
            JRserver = new JRadioButton();
            JRserver.setText("Server");   
            bg = new ButtonGroup();
            bg.add(JRclient);
            bg.add(JRserver);
            txtPortNumber = new JTextField();
            txtPortNumber.setText("Enter Port number");
            txtIP = new JTextField();
            txtIP.setText(adrs.getHostAddress());

    }
    
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
                      com.missileOutgoing = Integer.parseInt(ae.getActionCommand());  com.dataToSend = true; etat();
             }    
          });
        }    
    }
       
    private void CreateShips()
    {
        int head=0  ,nb = 0,v=0;
        Random r = new Random();
         Boolean bon =false;
        listJButtonInShip = new ArrayList<JButton>();
          listShip = new ArrayList<Ship>();
        
        for(int i = 0;i<5;i++)
        {
               head = RandomVal(0,98);
                if((head)%9==0 && head!=0){
                    head-=2;
                  
                          
                     }
                do{
                  nb = RandomVal(2,5);  
                   v = RandomVal(0,2); 
                }while(bon);
            try
            {
                 ArrayList<JButton> boatsection = new ArrayList<JButton>();
      if(v>0){       
                 for(int cnt=0;cnt< nb ;cnt++)
                 {   
                   boatsection.add(listbtn[head+cnt]);
                   listJButtonInShip.add(listbtn[head+cnt]);
                    if((head+cnt)%9==0 && head!=0){
                         break;
                     }
                 }
      }
      else{
      
         for(int cnt=0;cnt< nb ;cnt++)
                 {   
                   boatsection.add(listbtn[head+(cnt*9)]);
                   listJButtonInShip.add(listbtn[head+(cnt*9)]);   
                    if(head+(cnt*10)>90){
                         break;  
                     }
                 }
      }
                 Ship s = new Ship(boatsection,v);
                 listShip.add(s);
                 
            }
            catch(Exception ex)
            {
                
            }
     
        }

    }
    private static int RandomVal(int min, int max) {

	if (min >= max) {
		throw new IllegalArgumentException("Le max < min");
	}

	Random r = new Random();
	return r.nextInt((max - min) + 1) + min;
}
      public static int  VerifTir(int n){
          int retour=2;
          if(listbtn[n].getBackground()==java.awt.Color.green){
        listbtn[n].setBackground(java.awt.Color.red); 
        retour=1;
          }
          else if(listbtn[n].getBackground()==java.awt.Color.blue){
             listbtn[n].setBackground(java.awt.Color.yellow); 
              retour=0;
          }
       return  retour;
     }
    public static void UpdateGrig(int incomming)
    {
        Boolean missHit = true;
                  for(Ship element:listShip)
                  {
                      element.checkHit(listbtn[incomming]);
                     
                  }  
        
     
        for(JButton element:listJButtonInShip)
        {
            if(element.equals(listbtn[incomming])){
                missHit = false;}
        }

         if(missHit)
         {
            listbtn[incomming].setBackground(java.awt.Color.yellow); 
         }
    }

 
}
