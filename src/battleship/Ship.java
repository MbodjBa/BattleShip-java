/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javafx.scene.layout.Border;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

/**
 *
 * @author ftonye
 */
public class Ship
{
    
    public ArrayList<JButton> BoatSections;
    private int status;
    private int nbHit;
    private Boolean statvert;
    public Ship(ArrayList<JButton> BoatSection,int t)
    {
        this.BoatSections = new ArrayList<JButton>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbHit = 0;
        this.statvert=(t==0)?false:true;
     initShip();
    
    }
    
     public Ship(ArrayList<JButton> BoatSection)
    {
        this.BoatSections = new ArrayList<JButton>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbHit = 0; 
        initShip();  
    }
    
   public int getStatus()
   {
       return this.status;
   }
   
   public void initShip()
   {
       for(JButton section:this.BoatSections)
       {
         
            section.setBackground(java.awt.Color.green);
        } 
    }
   
   public void shipdeath(){
   
   if(this.nbHit>=this.BoatSections.size()){
     for(JButton section:this.BoatSections)
       {
               
           section.setBackground(java.awt.Color.BLACK);
       }
       }
   }
   
   public void checkHit(JButton cible)
   {
      
       for(JButton section:this.BoatSections)
       {
           if (section.equals(cible))
           {
             this.nbHit++;
             cible.setBackground(java.awt.Color.red);
           } 
                shipdeath();
       } 
   }    
}
